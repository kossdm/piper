#pragma once

#include "IOptions.h"
#include <string>

class TInitialOptions : public IOptions {
public:
    TInitialOptions(int argc, char* argv[]);

    const std::string& GetLeftType() const;
    const std::string& GetRightType() const;
private:
    std::string _leftType;
    std::string _rightType;
};