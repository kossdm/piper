cmake_minimum_required(VERSION 3.21)

project(options)

add_library(options TInitialOptions.cpp)
if(MSVC)
    set_property(TARGET options PROPERTY MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>")
endif(MSVC)