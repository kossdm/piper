#include "TInitialOptions.h"

#include "clara.hpp"

#include <string>
#include <iostream>

TInitialOptions::TInitialOptions(int argc, char* argv[]) {

   auto cli = clara::Opt(_leftType,  "left type") ["-lt"]["--left_type" ]("Left input device type") |
              clara::Opt(_rightType, "right type")["-rt"]["--right_type"]("Right input device type");

   auto result = cli.parse(clara::Args(argc, argv));
   if (!result) {
      std::cerr << "Error in command line: " << result.errorMessage() << std::endl;
      exit(1);
   }

   std::cout << "Utility start with params :\n";
   std::cout << " Left  type = " << _leftType  << "\n";
   std::cout << " Right type = " << _rightType << "\n";
}


const std::string& TInitialOptions::GetLeftType() const {
   return _leftType;
}

const std::string& TInitialOptions::GetRightType() const {
   return _rightType;
}