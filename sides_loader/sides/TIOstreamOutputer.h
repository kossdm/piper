#pragma once

#include "TSide.h"

class TIOstreamOutputer : public TSide {
public:
    void ReceiveFromOtherSide(const TDataMessage& message) override;
};
