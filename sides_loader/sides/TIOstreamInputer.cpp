#include "TIOstreamInputer.h"
#include <string>
#include <iostream>
#include "proto_generated/message.pb.h"

bool TIOstreamInputer::Work() {
    std::string data;
    std::cin >> data;

    TDataMessage message;
    message.set_string_data(data);
    std::cout << "send\n";
    SendToOtherSide(message);

    return true;
}