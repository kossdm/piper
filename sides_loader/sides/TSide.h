#pragma once
#include <sstream>

class TDataMessage;
class TSide {
    TSide* _side = nullptr;
public:
    void SetPartnerSide(TSide& side) { _side = &side; };
    TSide& Side() { return *_side; }
    void SendToOtherSide(const TDataMessage& message);
    virtual void ReceiveFromOtherSide(const TDataMessage& message) {};
    virtual bool Work() { return true; };
};
