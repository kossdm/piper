#include "TIOstreamOutputer.h"
#include <string>
#include <iostream>
#include "proto_generated/message.pb.h"

void TIOstreamOutputer::ReceiveFromOtherSide(const TDataMessage& message) {
    std::cout << message.string_data() << "\n";
}