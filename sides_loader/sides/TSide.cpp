#include "TSide.h"

void TSide::SendToOtherSide(const TDataMessage& message) {
    _side->ReceiveFromOtherSide(message);
};