#pragma once

#include "TSide.h"

class TIOstreamInputer : public TSide {
public:
    bool Work() override;
};
