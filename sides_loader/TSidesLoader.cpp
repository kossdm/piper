#include "TSidesLoader.h"

#include "options/TInitialOptions.h"
#include "sides/TIOstreamInputer.h"
#include "sides/TIOstreamOutputer.h"

#include <memory>
#include <iostream>

std::unique_ptr<TSide> CreateSide(const std::string& sideName) {
    if (sideName == "IOstreamInputer") {
        return std::make_unique<TIOstreamInputer>();
    } else if (sideName == "IOstreamOutputer") {
        return std::make_unique<TIOstreamOutputer>();
    }
    throw std::invalid_argument("Have not side with name " + sideName);
}

class TSidesLoaderImpl {
    std::unique_ptr<TSide> _leftSide;
    std::unique_ptr<TSide> _rightSide;
public:
    TSidesLoaderImpl(const TInitialOptions& options) :
       _leftSide(CreateSide(options.GetLeftType()))
     , _rightSide(CreateSide(options.GetRightType())) {

    _leftSide->SetPartnerSide(*_rightSide);
    _rightSide->SetPartnerSide(*_leftSide);

    }

    bool Work() {
        return _leftSide->Work() && _rightSide->Work();
    }

};

TSidesLoader::TSidesLoader(int argc, char* argv[]) {
    TInitialOptions options(argc, argv);
    _impl = new TSidesLoaderImpl(options);
}

bool TSidesLoader::Work() {
    return _impl->Work();
}