#pragma once

class TSidesLoaderImpl;

class TSidesLoader {
public:
    TSidesLoader(int argc, char* argv[]);
    bool Work();
private:
    TSidesLoaderImpl* _impl;
};
