#include "sides_loader/TSidesLoader.h"
#include <stdexcept>
#include <iostream>

int main(int argc, char* argv[]) {
    try {
        TSidesLoader moduleLoader(argc, argv);
        while(moduleLoader.Work()) {
        }
    }
    catch(const std::exception& e) {
        std::cerr << e.what() << '\n';
    }
    return 0;
}